//--------------------------------------------------------------------------------
// Arduboy experiment
// Dave Astels 2018
// MIT License  see LICENSE.md
//--------------------------------------------------------------------------------

#include <Arduboy.h>

Arduboy arduboy;

int playerwin;
int attempts;
int guessednumber;
int lastguess;
int randomnumber;

bool upbuffer;
bool downbuffer;
bool abuffer;


void setup()
{
  upbuffer = false;
  downbuffer = false;
  abuffer = false;
  arduboy.begin();
  arduboy.clear();
  playerwin = 0;
  attempts = 0;
  guessednumber = 0;
  lastguess = 0;
  srand(7/8);
  randomnumber = 1 + rand() % 100;
}


void loop()
{
  arduboy.clear();
  if (playerwin == 0) {
    if (attempts == 7) {
      arduboy.setCursor(0, 0);
      arduboy.print("You lost!");
      arduboy.print("\n");
      arduboy.print("Correct Number: ");
      arduboy.print(randomnumber);
      if (arduboy.pressed(A_BUTTON) && !abuffer) {
        abuffer = true;
        randomnumber = 1 + rand() % 100;
        guessednumber = 0;
        lastguess = 0;
        attempts = 0;
        playerwin = 0;
      }

    } else {
      if (arduboy.pressed(UP_BUTTON) && !upbuffer) {
        upbuffer = true;
        guessednumber++;
      }
      if (arduboy.pressed(DOWN_BUTTON) && !downbuffer) {
        downbuffer = true;
        guessednumber--;
      }
      if (arduboy.pressed(A_BUTTON) && !abuffer) {
        abuffer = true;
        if (guessednumber == randomnumber) {
          playerwin = 1;
        } else {
          attempts++;
          lastguess = guessednumber;
        }
      }
      arduboy.setCursor(0, 0);
      arduboy.print("Attempt: ");
      arduboy.print(attempts);
      arduboy.print("\n");
      arduboy.print("Number to guess: ");
      arduboy.print(guessednumber);
      arduboy.print("\n");
      if (attempts == 0) {
        arduboy.print("Good luck!");
      } else {
        arduboy.print(lastguess);
        if (lastguess > randomnumber) {
          arduboy.print(" is too high!");
        } else if (lastguess < randomnumber) {
          arduboy.print(" is too low!");
        }
      }
    }
  } else {
    arduboy.setCursor(0, 0);
    arduboy.print("You won!");
    arduboy.print("\n");
    arduboy.print("Correct Number: ");
    arduboy.print(randomnumber);
    if (arduboy.pressed(A_BUTTON) && !abuffer) {
      abuffer = true;
      randomnumber = 1 + rand() % 100;
      attempts = 0;
      playerwin = 0;
    }
  }
  
  arduboy.display();

  if (arduboy.notPressed(A_BUTTON)) {
    abuffer = false;
  }
  if (arduboy.notPressed(DOWN_BUTTON)) {
    downbuffer = false;
  }
  if (arduboy.notPressed(UP_BUTTON)) {
    upbuffer = false;
  }
}